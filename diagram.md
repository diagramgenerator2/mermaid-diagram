This is sample diahram to render on gitlab.

```mermaid
graph LR

A[Start] --> B(Enter Loan Details)
B --> C{Is Loan Eligible?}
C -- Yes --> D(Send Loan Confirmation)
C -- No --> E(Reject Loan)
D --> F(Enter Payment Details)
F --> G(Validate Payment)
G -- Valid --> H(Successful Payment)
G -- Invalid --> I(Failed Payment)
H --> J(Complete Transaction)
I --> J
J --> K(End)

E --> K
```
